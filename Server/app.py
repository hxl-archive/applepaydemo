#!flask/bin/python
# A simple server for Braintree sandbox.
# See http://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask
from flask import Flask, request
import braintree

app = Flask(__name__)
braintree.Configuration.configure(braintree.Environment.Sandbox,
                                  merchant_id="b87v8pwh46qzypyc",
                                  public_key="q2g65bpxy2y4rfzq",
                                  private_key="22daa56b995f7b1f4fc09cb917d439d5")

@app.route('/')
def index():
    return "Hello, World!"
    
@app.route("/client_token", methods=["GET"])
def client_token():
	return braintree.ClientToken.generate()
	
@app.route("/checkout", methods=["POST"])
def checkout():
	nonce = request.form["payment_method_nonce"]
	result = braintree.Transaction.sale({
		"amount": "0.01",
		"payment_method_nonce": nonce
	})
	return "checkout_done"

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

