//
//  APDPayItemListViewController.m
//  ApplePayDemo
//
//  Created by Haoxin Li on 9/3/15.
//  Copyright (c) 2015 Haoxin Li. All rights reserved.
//

#import "APDConstants.h"
#import "APDPayItemListViewController.h"
#import <Braintree/Braintree.h>
#import <PassKit/PassKit.h>

#define API_SERVER (@"http://192.168.1.70:5000")	// hli: This is a dynamic IP that will change over time. Check this if could not connect to server.

@interface APDPayItemListViewController ()
<
PKPaymentAuthorizationViewControllerDelegate
>

@property (nonatomic, strong) Braintree *braintree;

@end

@implementation APDPayItemListViewController

- (void)viewDidLoad {
	[super viewDidLoad];
}

- (IBAction)didHitBuyButton:(UIButton *)sender {
	
	PKPaymentRequest *paymentRequest = [PKPaymentRequest new];
	paymentRequest.merchantIdentifier = @"merchant.com.haoxinli.braintree-sandbox";
	paymentRequest.supportedNetworks = @[PKPaymentNetworkVisa];
	paymentRequest.merchantCapabilities = PKMerchantCapability3DS;
	paymentRequest.countryCode = @"US";
	paymentRequest.currencyCode = @"USD";
	paymentRequest.requiredShippingAddressFields = PKAddressFieldAll;
	
	paymentRequest.shippingMethods = [self shippingMethods];
	paymentRequest.paymentSummaryItems =[self paymentSummaryItemsForShippingMethod:paymentRequest.shippingMethods[0]];
	
	
	if ([PKPaymentAuthorizationViewController canMakePaymentsUsingNetworks:paymentRequest.supportedNetworks]) {
		PKPaymentAuthorizationViewController *paymentAuthorizationVC = [[PKPaymentAuthorizationViewController alloc] initWithPaymentRequest:paymentRequest];
		paymentAuthorizationVC.delegate = self;
		[self presentViewController:paymentAuthorizationVC animated:YES completion:nil];
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
														message:@"You are unable to make payment now."
													   delegate:nil
											  cancelButtonTitle:nil
											  otherButtonTitles:@"OK", nil];
		[alert show];
	}
}

- (NSArray *)shippingMethods {
	
	// shipping methods
	
	PKShippingMethod *freeShipping = [PKShippingMethod new];
	freeShipping.label = @"Free Shipping";
	freeShipping.amount = [[NSDecimalNumber alloc] initWithString:@"0"];
	freeShipping.identifier = @"freeShipping";
	freeShipping.detail = @"Arrives in 6-8 weeks";
	
	PKShippingMethod *expressShipping = [PKShippingMethod new];
	expressShipping.label = @"Express Shipping";
	expressShipping.amount = [[NSDecimalNumber alloc] initWithString:@"0.10"];
	expressShipping.identifier = @"expressShipping";
	expressShipping.detail = @"Arrives in 2-3 days";
	
	return @[freeShipping, expressShipping];
}

- (NSArray *)paymentSummaryItemsForShippingMethod:(PKShippingMethod *)shippingMethod {
	
	// payment details
	
	PKPaymentSummaryItem *item0 = [[PKPaymentSummaryItem alloc] init];
	item0.label = @"Test Item 0";
	item0.amount = [[NSDecimalNumber alloc] initWithString:@"0.02"];
	
	PKPaymentSummaryItem *item1 = [[PKPaymentSummaryItem alloc] init];
	item1.label = @"Discount";
	item1.amount = [[NSDecimalNumber alloc] initWithString:@"-0.01"];
	
	PKPaymentSummaryItem *total = [[PKPaymentSummaryItem alloc] init];
	total.label = @"Total";
	total.amount = [item0.amount decimalNumberByAdding:item1.amount];
	
	return @[item0, item1, shippingMethod, total];	// hli: The last item is the actual total, regardless of what the previous item numbers are!
}

#pragma mark - PKPaymentAuthorizationViewControllerDelegate -

- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
					   didAuthorizePayment:(PKPayment *)payment
								completion:(void (^)(PKPaymentAuthorizationStatus status))completion {
	
	NSLog(@"%s %@ %@ %@ %@", __PRETTY_FUNCTION__, payment.token, payment.billingAddress, payment.shippingAddress, payment.shippingMethod);
	
	NSURL *tokenURL = [NSURL URLWithString:[API_SERVER stringByAppendingPathComponent:@"client_token"]];
	NSMutableURLRequest *tokenRequest = [NSMutableURLRequest requestWithURL:tokenURL];
	[NSURLConnection sendAsynchronousRequest:tokenRequest
									   queue:[NSOperationQueue mainQueue]
						   completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
							   if (connectionError) {
								   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
																				   message:[connectionError localizedDescription]
																				  delegate:nil
																		 cancelButtonTitle:nil
																		 otherButtonTitles:@"OK", nil];
								   [alert show];
								   completion(PKPaymentAuthorizationStatusFailure);
							   }
							   else {
								   NSString *responseBody = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
								   self.braintree = [Braintree braintreeWithClientToken:responseBody];
								   [self.braintree tokenizeApplePayPayment:payment completion:^(NSString * _Nullable nonce, NSError * _Nullable error) {
									   if (error) {
										   // Received an error from Braintree.
										   // Indicate failure via the completion callback.
										   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
																						   message:[error localizedDescription]
																						  delegate:nil
																				 cancelButtonTitle:nil
																				 otherButtonTitles:@"OK", nil];
										   [alert show];
										   completion(PKPaymentAuthorizationStatusFailure);
										   return;
									   }
									   
									   NSURL *paymentURL = [NSURL URLWithString:[API_SERVER stringByAppendingPathComponent:@"checkout"]];
									   NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:paymentURL];
									   request.HTTPMethod = @"POST";
									   request.HTTPBody = [[NSString stringWithFormat:@"payment_method_nonce=%@", nonce] dataUsingEncoding:NSUTF8StringEncoding];
									   [NSURLConnection sendAsynchronousRequest:request
																		  queue:[NSOperationQueue mainQueue]
															  completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
																  if (error) {
																	  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
																													  message:[error localizedDescription]
																													 delegate:nil
																											cancelButtonTitle:nil
																											otherButtonTitles:@"OK", nil];
																	  [alert show];
																	  completion(PKPaymentAuthorizationStatusFailure);
																  }
																  else {
																	  NSString *responseBody = [[NSString alloc] initWithData:data
																													 encoding:NSUTF8StringEncoding];
																	  NSLog(@"Success: %@", responseBody);
																	  completion(PKPaymentAuthorizationStatusSuccess);
																  }
															  }];
								   }];
							   }
						   }];
}

- (void)paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)controller {
	NSLog(@"%s", __PRETTY_FUNCTION__);
	[controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)paymentAuthorizationViewControllerWillAuthorizePayment:(PKPaymentAuthorizationViewController *)controller {
	NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
				   didSelectShippingMethod:(PKShippingMethod *)shippingMethod
								completion:(void (^)(PKPaymentAuthorizationStatus status, NSArray<PKPaymentSummaryItem *> *summaryItems))completion {
	NSLog(@"%s", __PRETTY_FUNCTION__);
	completion(PKPaymentAuthorizationStatusSuccess, [self paymentSummaryItemsForShippingMethod:shippingMethod]);
}

- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
				  didSelectShippingAddress:(ABRecordRef)address
								completion:(nonnull void (^)(PKPaymentAuthorizationStatus, NSArray<PKShippingMethod *> * _Nonnull, NSArray<PKPaymentSummaryItem *> * _Nonnull))completion {
	NSLog(@"%s", __PRETTY_FUNCTION__);
	NSArray *shippingMethods = [self shippingMethods];
	completion(PKPaymentAuthorizationStatusSuccess, shippingMethods, [self paymentSummaryItemsForShippingMethod:shippingMethods[0]]);
}

@end
